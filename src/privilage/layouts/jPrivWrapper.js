/* eslint-disable import/no-duplicates,import/prefer-default-export,object-shorthand */
/* eslint-disable no-unused-expressions,no-unused-vars,key-spacing */

const adminBudget = [
  {
    parent: {
      name: 'COST CENTER',
      icon: 'building',
      hrefout: '/costcenter/#/dashboard/budget-transfer/list/sent',
      isShow: true,
    },
    child: [
      {
        name: 'APPROVAL',
        // href: '/dashboard/config',
        href: null,
        hrefout: '/costcenter/#/dashboard/budget-transfer/list/sent',
        icon: 'list-ul',
        isShow: true,
      },
      {
        name: 'CONFIG',
        // href: '/dashboard/config',
        href: null,
        hrefout: '/costcenter/#/dashboard/config',
        icon: 'wrench',
        isShow: true,
      },
      {
        name: 'LIST',
        href: null,
        hrefout: '/costcenter/#/dashboard/cost-center',
        icon: 'list-ul',
        isShow: true,
      },
      {
        name: 'REPORT',
        href: null,
        hrefout: '/costcenter/#/dashboard/report',
        icon: 'list-ul',
        isShow: true,
      },
    ],
  },
];

const admin = [
  {
    parent: {
      name: 'HOME',
      icon: 'file-text-o',
      isShow: true,
    },
    child: [
      {
        name: 'MY HOME',
        href: '/home',
        icon: 'calendar-minus-o',
        isShow: true,
      },
      {
        name: 'DASHBOARD',
        href: '/dashboard',
        icon: 'calendar-minus-o',
        isShow: true,
      },
      {
        name: 'CHALLANGE',
        href: '/challange/list',
        icon: 'calendar-minus-o',
        isShow: true,
      },
    ],
  },
];

export const jPrivWrapper = {
  created() {
    const role = JSON.parse(localStorage.getItem('role'));
    // const roleString = JSON.parse(localStorage.getItem('roleString'));
    if (role.includes(2)) {
      this.menusDetails.push(...adminBudget);
      this.menusDetails.push(...admin);
    } else if (role.includes(1) && role.includes(17)) {
      this.menusDetails.push(...adminBudget);
      this.menusDetails.push(...admin);
    } else if (role.includes(1)) {
      // this.menusDetails.push(...adminBudget);
      this.menusDetails.push(...admin);
    } else if (role.includes(17)) {
      this.menusDetails.push(...adminBudget);
    }
  },
};
