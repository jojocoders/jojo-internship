import jHome from '@/views/home/jHome';
import auth from '@/middleware/auth';
import log from '@/middleware/log';
import LIST from '@/views/challange/jChallangeList';

export default [
  {
    path: '/home',
    name: 'jHome1',
    component: jHome,
    meta: {
      title: 'Home',
      middleware: [auth, log],
      layout: 'jWrapper',
    },
  },
  {
    path: '/dashboard',
    name: 'jHome2',
    component: jHome,
    meta: {
      title: 'Home',
      middleware: [auth, log],
      layout: 'jWrapper',
    },
  },
  {
    path: '/challange/list',
    name: 'jHome3',
    component: LIST,
    meta: {
      title: 'Home',
      middleware: [auth, log],
      layout: 'jWrapper',
    },
  },
];
