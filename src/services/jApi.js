import axios from 'axios';
import Vue from 'vue';
import stores from '@/stores';
import router from '@/router';
import jError from '@/error/jError';

export default {
  generateApi() {
    const token = Vue.ls.get('Token');

    const api = axios.create({
      baseURL: process.env.GATE_URL,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });

    api.interceptors.response.use(
      (response) => {
        if (typeof response.headers.token !== 'undefined') {
          stores.commit('jStoreLogin/saveToken', response.headers);
          stores.commit('jStoreLogin/setInLocalStorage');
        }
        return response;
      },
      (error) => {
        if (typeof error.response.data.session_expired === 'boolean') {
          if (error.response.data.session_expired === true) {
            router.push('/logout');
          }
        } else if (error.response.status === 406 || error.response.status === 401) {
          router.push('/logout');
        }
        jError.getErrorMessage(error.response.data);
        // handle error
        return Promise.reject(error);
      });

    return api;
  },
  loginApi() {
    const api = axios.create({
      baseURL: process.env.GATE_URL,
    });
    return api;
  },
};
