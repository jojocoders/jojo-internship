import jApi from './jApi';

export default {
  login(authData) {
    const api = jApi.loginApi();
    return api.post('auth/login', authData)
      .then(res => res.data);
  },
  refreshToken(req) {
    const api = jApi.loginApi();
    return api.post('auth/refresh', req)
      .then(res => res.data);
  },
};
