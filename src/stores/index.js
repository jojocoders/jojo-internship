import Vue from 'vue';
import Vuex from 'vuex';

// Modules import downhere
import jStoreLogin from './modules/jStoreLogin';
import jStoreNotificationScreen from './modules/jStoreNotificationScreen';

// Initialize vuex
Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    // Include imported modules in here
    jStoreLogin,
    jStoreNotificationScreen,
  },
  strict: debug,
});
