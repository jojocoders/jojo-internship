/* eslint-disable no-shadow,consistent-return,no-param-reassign */

const state = {
  // Set default state(s) here
  info_screen: false,
  info_message: '',

  problem_screen: false,
  problem_message: '',
  problem_code: '',

  success_screen: false,
  success_message: '',

  loading_screen: false,
  loading_message: '',
};

const getters = {
  // Change value of state(s) with getter

};

const mutations = {
  toggleStateInfo(state, message) {
    state.info_screen = !state.info_screen;
    if (message) {
      state.info_message = message;
    }
  },
  toggleStateProblem(state, message) {
    state.problem_screen = !state.problem_screen;
    if (message) {
      if (message.error_code !== undefined) {
        state.problem_message = message.error_message;
        state.problem_code = message.error_code;
      } else {
        state.problem_message = message;
        state.problem_code = '';
      }
    }
  },
  toggleStateSuccess(state, message) {
    state.success_screen = !state.success_screen;
    if (message) {
      state.success_message = message;
    }
  },
  toggleStateLoading(state, message) {
    state.loading_screen = !state.loading_screen;
    if (message) {
      state.loading_message = message;
    }
  },
  showStateLoading(state, message) {
    state.loading_screen = true;
    if (message) {
      state.loading_message = message;
    }
  },
  hideStateLoading(state, message) {
    state.loading_screen = false;
    if (message) {
      state.loading_message = message;
    }
  },
};

const actions = {
  toggleInfo({ commit }, message) {
    commit('toggleStateInfo', message);
  },
  toggleProblem({ commit }, message) {
    commit('toggleStateProblem', message);
  },
  toggleSuccess({ commit }, message) {
    commit('toggleStateSuccess', message);
  },
  toggleLoading({ commit }, message) {
    commit('toggleStateLoading', message);
  },
  hideLoading({ commit }, message) {
    commit('hideStateLoading', message);
  },
  showLoading({ commit }, message) {
    commit('showStateLoading', message);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
